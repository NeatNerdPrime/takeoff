# Deploying from a private repository

Describes the additional steps required for deploying from a private repository.
This is a complementary guide to the documentation for [deploying GitLab](README.md#deploying-gitlab).

### Requirements

- Login credentials for https://packages.gitlab.com/

#### 1. Locate the private repository

Log in to https://packages.gitlab.com. Go to the repository for the
security release (usually named with the prefix `security-`) and look for the
`ubuntu/xenial` (Ubuntu 16.04) EE package. Click to open the details view of the package.

#### 2. Install the package on the target workers

In the "Install this Package" section, you will see a line as follow:

```
curl -s https://[KEY-STRING]:@packages.gitlab.com/install/repositories/gitlab/security-[REPO-CODE]/script.deb.sh | sudo bash
```

Copy the KEY-STRING and set the `key` attribute in the `gitlab-cluster-base` role:

```
bundle exec rake edit_role_secrets[gitlab-cluster-base]
....
{
  "id": "_default",
  "omnibus-gitlab": {
    "package": {
      "key": "KEY-STRING"
    },
.....
```

#### 3. Find the correct `repo` & `version` strings to deploy

You'll also see the following line:
```
sudo apt-get install gitlab-ee=8.13.3-ee.0
```

In this case, the values you'll need while updating the role definition are the following:
* repo: `security-[REPO-CODE]`
* use_key: `true`
* version: `8.13.3-ee.0`

Now you can keep following the instructions for [deploying](deploying.md)
or [deploying to staging](staging.md), according to your needs.
