require 'monitor'
require 'json'
require 'forwardable'

class Resumer
  include MonitorMixin
  extend Forwardable

  STATE_FILE = '.resume'.freeze
  State = Struct.new(:current_step, :substeps, :start_time)

  def_delegator :@state, :start_time

  def initialize(resume)
    super()

    @state = create_state(resume)
  end

  def set_current_step(step_number)
    self.synchronize do
      @state.current_step = step_number

      save
    end
  end

  def add_substep(substep)
    self.synchronize do
      @state.substeps << substep

      save
    end
  end

  def has_substep?(substep)
    self.synchronize do
      @state.substeps.include?(substep)
    end
  end

  def current_step
    self.synchronize do
      @state.current_step
    end
  end

  def success
    self.synchronize do
      File.delete(STATE_FILE) if File.exist?(STATE_FILE)
    end
  end

  def skip?(step)
    current_step > step
  end

  private

  def create_state(resume)
    if resume && File.exist?(STATE_FILE)
      json_state = JSON.parse(File.read(STATE_FILE), symbolize_names: true)
      json_state[:start_time] = Time.parse(json_state[:start_time]).utc

      State.new(*json_state.values)
    else
      State.new(0, [], Time.now.utc)
    end
  end

  def save
    File.open(STATE_FILE, 'w') { |f| f.write(@state.to_h.to_json) }
  end
end
