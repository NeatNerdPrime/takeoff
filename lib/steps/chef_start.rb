# frozen_string_literal: true

require './lib/steps/base_roles'

module Steps
  class ChefStart < BaseRoles
    def run
      run_command_on_roles roles_to_start,
                           chef_command,
                           title: title
    end

    private

    def roles_to_start
      options[:roles_to_start] ||= roles.blessed_node
    end

    def title
      options[:roles_to_start] ? "Running chef-client on #{Roles.short_output(roles_to_start)}" : 'Installing the package on the blessed node'
    end

    def chef_command
      options[:roles_to_start].is_a?(Array) ? 'sudo -E service chef-client start' : 'sudo -E chef-client'
    end
  end
end
