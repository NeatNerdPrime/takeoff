# frozen_string_literal: true

require 'rspec'
require './config/roles'

describe Roles do
  subject { described_class.new('gstg') }

  it 'returns the service roles' do
    expect(subject.service_roles).not_to be_empty
  end

  it 'returns the regular roles' do
    expect(subject.regular_roles).not_to be_empty
  end

  it 'returns the sidekiq roles' do
    expect(subject.sidekiq_roles).not_to be_empty
  end

  it 'returns the services for a role' do
    expect(subject.services_for_role('gstg-base-fe-web')).not_to be_empty
  end

  it 'returns the regular and blessed roles' do
    expect(subject.regular_and_blessed).not_to be_empty
  end
end
