# frozen_string_literal: true

require 'spec_helper'
require './lib/lb/command_wrapper'
require './lib/lb/config'
require './lib/steps/deploy_mediator'

describe Steps::DeployMediator do
  before do
    enable_dry_run

    allow_any_instance_of(Lb::Config).to receive(:lb_list).and_return('lb-1:lb-2')

    # Avoid Threads here so the output is not random
    allow_any_instance_of(described_class).to receive(:run_steps!) do |mediator|
      mediator.send(:concurrent_steps).map { |step| step.run! }
    end
  end

  subject do
    described_class.new(Roles.new('gstg'),
                        version: '10.0.2-ee.0',
                        resumer: Resumer.new(false),
                        command_wrapper: Lb::CommandWrapper.new('gstg'))
  end

  context 'no custom node' do
    it 'outputs the right command' do
      expect { subject.run }.to output(command).to_stdout_from_any_process
    end

    def command
      command = <<~COMMAND
        bundle exec knife search 'roles:gstg-base-fe-registry' -a fqdn -Fjson
        bundle exec knife search 'roles:gstg-base-deploy-node' -a fqdn -Fjson
        bundle exec knife ssh -e -a fqdn 'fqdn:fqdn1' 'sudo cat /opt/gitlab/embedded/service/gitlab-rails/GITLAB_PAGES_VERSION 2>/dev/null'
        bundle exec knife ssh -e -a fqdn 'fqdn:fqdn2' 'sudo cat /opt/gitlab/embedded/service/gitlab-rails/GITLAB_PAGES_VERSION 2>/dev/null'
        bundle exec knife search 'roles:gstg-base-fe-registry' -a fqdn -Fjson
        bundle exec knife search 'roles:gstg-base-deploy-node' -a fqdn -Fjson
        bundle exec knife ssh -e -a fqdn 'fqdn:fqdn1' 'sudo cat /opt/gitlab/embedded/service/gitlab-rails/GITLAB_WORKHORSE_VERSION 2>/dev/null'
        bundle exec knife ssh -e -a fqdn 'fqdn:fqdn2' 'sudo cat /opt/gitlab/embedded/service/gitlab-rails/GITLAB_WORKHORSE_VERSION 2>/dev/null'
        bundle exec knife ssh -e -a fqdn roles:gstg-base-be-sidekiq sudo apt-get install -y -q --force-yes gitlab-ee=10.0.2-ee.0 && sudo gitlab-ctl reconfigure && sudo gitlab-ctl hup unicorn
        bundle exec knife ssh -e -a fqdn roles:gstg-base-be-sidekiq sudo gitlab-ctl restart sidekiq-cluster
        bundle exec knife ssh -e -a fqdn roles:gstg-base-be-sidekiq sudo gitlab-ctl restart nginx
        bundle exec knife ssh -e -a fqdn roles:gstg-base-be-mailroom sudo apt-get install -y -q --force-yes gitlab-ee=10.0.2-ee.0 && sudo gitlab-ctl reconfigure
        bundle exec knife ssh -e -a fqdn roles:gstg-base-be-mailroom sudo gitlab-ctl restart
        bundle exec knife ssh -e -C 3 -a fqdn roles:gstg-base-fe-web sudo /usr/local/bin/ha-ctl -l lb-1:lb-2 -c "sudo apt-get install -y -q --force-yes gitlab-ee=10.0.2-ee.0 && sudo gitlab-ctl reconfigure && sudo gitlab-ctl hup unicorn && sleep 3"
        bundle exec knife ssh -e -a fqdn roles:gstg-base-fe-web sudo gitlab-ctl restart deploy-page
        bundle exec knife ssh -e -C 3 -a fqdn roles:gstg-base-fe-web sudo /usr/local/bin/ha-ctl -l lb-1:lb-2 -c "sudo gitlab-ctl restart gitlab-workhorse && sleep 3"
        bundle exec knife ssh -e -C 3 -a fqdn roles:gstg-base-fe-web sudo /usr/local/bin/ha-ctl -l lb-1:lb-2 -c "sudo gitlab-ctl restart nginx && sleep 3"
        bundle exec knife ssh -e -C 1 -a fqdn roles:gstg-base-fe-web-pages sudo /usr/local/bin/ha-ctl -l lb-1:lb-2 -c "sudo apt-get install -y -q --force-yes gitlab-ee=10.0.2-ee.0 && sudo gitlab-ctl reconfigure && sleep 3"
        bundle exec knife ssh -e -C 1 -a fqdn roles:gstg-base-fe-web-pages sudo /usr/local/bin/ha-ctl -l lb-1:lb-2 -c "sudo gitlab-ctl restart gitlab-pages && sleep 3"
        bundle exec knife ssh -e -C 3 -a fqdn roles:gstg-base-fe-api sudo /usr/local/bin/ha-ctl -l lb-1:lb-2 -c "sudo apt-get install -y -q --force-yes gitlab-ee=10.0.2-ee.0 && sudo gitlab-ctl reconfigure && sudo gitlab-ctl hup unicorn && sleep 3"
        bundle exec knife ssh -e -C 3 -a fqdn roles:gstg-base-fe-api sudo /usr/local/bin/ha-ctl -l lb-1:lb-2 -c "sudo gitlab-ctl restart gitlab-workhorse && sleep 3"
        bundle exec knife ssh -e -C 3 -a fqdn roles:gstg-base-fe-api sudo /usr/local/bin/ha-ctl -l lb-1:lb-2 -c "sudo gitlab-ctl restart nginx && sleep 3"
        bundle exec knife ssh -e -a fqdn roles:gstg-base-fe-api sudo gitlab-ctl restart registry
        bundle exec knife ssh -e -C 3 -a fqdn roles:gstg-base-fe-git sudo /usr/local/bin/ha-ctl -l lb-1:lb-2 -c "sudo apt-get install -y -q --force-yes gitlab-ee=10.0.2-ee.0 && sudo gitlab-ctl reconfigure && sudo gitlab-ctl hup unicorn && sleep 3"
        bundle exec knife ssh -e -C 3 -a fqdn roles:gstg-base-fe-git sudo /usr/local/bin/ha-ctl -l lb-1:lb-2 -c "sudo gitlab-ctl restart gitlab-workhorse && sleep 3"
        bundle exec knife ssh -e -C 3 -a fqdn roles:gstg-base-fe-git sudo /usr/local/bin/ha-ctl -l lb-1:lb-2 -c "sudo gitlab-ctl restart nginx && sleep 3"
        bundle exec knife ssh -e -C 3 -a fqdn roles:gstg-base-fe-registry sudo /usr/local/bin/ha-ctl -l lb-1:lb-2 -c "sudo apt-get install -y -q --force-yes gitlab-ee=10.0.2-ee.0 && sudo gitlab-ctl reconfigure && sleep 3"
        bundle exec knife ssh -e -a fqdn roles:gstg-base-fe-registry sudo gitlab-ctl restart
      COMMAND
    end
  end

  context 'with custom node' do
    it 'outputs the right command' do
      stub_const('Steps::ServicesRestart::CUSTOM_RESTART_NODE', 'gstg-base-fe-web')

      expect { subject.run }.to output(command).to_stdout_from_any_process
    end

    def command
      command = <<~COMMAND
        bundle exec knife search 'roles:gstg-base-fe-registry' -a fqdn -Fjson
        bundle exec knife search 'roles:gstg-base-deploy-node' -a fqdn -Fjson
        bundle exec knife ssh -e -a fqdn 'fqdn:fqdn1' 'sudo cat /opt/gitlab/embedded/service/gitlab-rails/GITLAB_PAGES_VERSION 2>/dev/null'
        bundle exec knife ssh -e -a fqdn 'fqdn:fqdn2' 'sudo cat /opt/gitlab/embedded/service/gitlab-rails/GITLAB_PAGES_VERSION 2>/dev/null'
        bundle exec knife search 'roles:gstg-base-fe-registry' -a fqdn -Fjson
        bundle exec knife search 'roles:gstg-base-deploy-node' -a fqdn -Fjson
        bundle exec knife ssh -e -a fqdn 'fqdn:fqdn1' 'sudo cat /opt/gitlab/embedded/service/gitlab-rails/GITLAB_WORKHORSE_VERSION 2>/dev/null'
        bundle exec knife ssh -e -a fqdn 'fqdn:fqdn2' 'sudo cat /opt/gitlab/embedded/service/gitlab-rails/GITLAB_WORKHORSE_VERSION 2>/dev/null'
        bundle exec knife ssh -e -a fqdn roles:gstg-base-be-sidekiq sudo apt-get install -y -q --force-yes gitlab-ee=10.0.2-ee.0 && sudo gitlab-ctl reconfigure && sudo gitlab-ctl hup unicorn
        bundle exec knife ssh -e -a fqdn roles:gstg-base-be-sidekiq sudo gitlab-ctl restart sidekiq-cluster
        bundle exec knife ssh -e -a fqdn roles:gstg-base-be-sidekiq sudo gitlab-ctl restart nginx
        bundle exec knife ssh -e -a fqdn roles:gstg-base-be-mailroom sudo apt-get install -y -q --force-yes gitlab-ee=10.0.2-ee.0 && sudo gitlab-ctl reconfigure
        bundle exec knife ssh -e -a fqdn roles:gstg-base-be-mailroom sudo gitlab-ctl restart
        bundle exec knife ssh -e -C 3 -a fqdn roles:gstg-base-fe-web sudo /usr/local/bin/ha-ctl -l lb-1:lb-2 -c "sudo apt-get install -y -q --force-yes gitlab-ee=10.0.2-ee.0 && sudo gitlab-ctl reconfigure && sudo gitlab-ctl hup unicorn && sleep 3"
        bundle exec knife ssh -e -a fqdn roles:gstg-base-fe-web sudo gitlab-ctl restart deploy-page
        bundle exec knife ssh -e -C 3 -a fqdn roles:gstg-base-fe-web sudo /usr/local/bin/ha-ctl -l lb-1:lb-2 -c "sudo gitlab-ctl restart gitlab-workhorse && sleep 3"
        bundle exec knife ssh -e -C 3 -a fqdn roles:gstg-base-fe-web sudo /usr/local/bin/ha-ctl -l lb-1:lb-2 -c "sudo gitlab-ctl restart nginx && sleep 3"
        bundle exec knife ssh -e -C 1 -a fqdn roles:gstg-base-fe-web-pages sudo /usr/local/bin/ha-ctl -l lb-1:lb-2 -c "sudo apt-get install -y -q --force-yes gitlab-ee=10.0.2-ee.0 && sudo gitlab-ctl reconfigure && sleep 3"
        bundle exec knife ssh -e -C 1 -a fqdn roles:gstg-base-fe-web-pages sudo /usr/local/bin/ha-ctl -l lb-1:lb-2 -c "sudo gitlab-ctl restart gitlab-pages && sleep 3"
        bundle exec knife ssh -e -C 3 -a fqdn roles:gstg-base-fe-api sudo /usr/local/bin/ha-ctl -l lb-1:lb-2 -c "sudo apt-get install -y -q --force-yes gitlab-ee=10.0.2-ee.0 && sudo gitlab-ctl reconfigure && sudo gitlab-ctl hup unicorn && sleep 3"
        bundle exec knife ssh -e -C 3 -a fqdn roles:gstg-base-fe-api sudo /usr/local/bin/ha-ctl -l lb-1:lb-2 -c "sudo gitlab-ctl restart gitlab-workhorse && sleep 3"
        bundle exec knife ssh -e -C 3 -a fqdn roles:gstg-base-fe-api sudo /usr/local/bin/ha-ctl -l lb-1:lb-2 -c "sudo gitlab-ctl restart nginx && sleep 3"
        bundle exec knife ssh -e -a fqdn roles:gstg-base-fe-api sudo gitlab-ctl restart registry
        bundle exec knife ssh -e -C 3 -a fqdn roles:gstg-base-fe-git sudo /usr/local/bin/ha-ctl -l lb-1:lb-2 -c "sudo apt-get install -y -q --force-yes gitlab-ee=10.0.2-ee.0 && sudo gitlab-ctl reconfigure && sudo gitlab-ctl hup unicorn && sleep 3"
        bundle exec knife ssh -e -C 3 -a fqdn roles:gstg-base-fe-git sudo /usr/local/bin/ha-ctl -l lb-1:lb-2 -c "sudo gitlab-ctl restart gitlab-workhorse && sleep 3"
        bundle exec knife ssh -e -C 3 -a fqdn roles:gstg-base-fe-git sudo /usr/local/bin/ha-ctl -l lb-1:lb-2 -c "sudo gitlab-ctl restart nginx && sleep 3"
        bundle exec knife ssh -e -C 3 -a fqdn roles:gstg-base-fe-registry sudo /usr/local/bin/ha-ctl -l lb-1:lb-2 -c "sudo apt-get install -y -q --force-yes gitlab-ee=10.0.2-ee.0 && sudo gitlab-ctl reconfigure && sleep 3"
        bundle exec knife ssh -e -a fqdn roles:gstg-base-fe-registry sudo gitlab-ctl restart
      COMMAND
    end
  end
end
