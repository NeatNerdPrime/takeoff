# frozen_string_literal: true

require 'spec_helper'
require './lib/steps/migrations'

describe Steps::Migrations do
  before { enable_dry_run }

  context 'with migrations' do
    subject { described_class.new(Roles.new('gstg')) }

    it 'outputs the right command' do
      command = <<~COMMAND
        bundle exec knife ssh -e -a fqdn roles:gstg-base-deploy-node sudo SKIP_POST_DEPLOYMENT_MIGRATIONS=1 gitlab-rake db:migrate
      COMMAND

      expect { subject.run }.to output(command).to_stdout_from_any_process
    end
  end

  context 'without migrations' do
    subject { described_class.new(Roles.new('gstg')) }

    it 'outputs the right command' do
      allow(subject).to receive(:run_migrations?).and_return(false)

      expect { subject.run }.to output('').to_stdout_from_any_process
    end
  end
end
