# frozen_string_literal: true

require 'spec_helper'
require './lib/steps/post_deploy_mediator'
require './lib/lb/config'
require './lib/lb/command_wrapper'

describe Steps::PostDeployMediator do
  before do
    enable_dry_run

    allow_any_instance_of(Lb::Config).to receive(:lb_list).and_return('lb-1:lb-2')
  end

  subject do
    described_class.new(Roles.new('gstg'),
                        command_wrapper: Lb::CommandWrapper.new('gstg'))
  end

  context 'no custom node' do
    it 'outputs the right command' do
      expect { subject.run && $stdout.flush }.to output(command).to_stdout_from_any_process
    end

    def command
      command = <<~COMMAND
        bundle exec knife ssh -e -C 3 -a fqdn roles:gstg-base-fe-api OR roles:gstg-base-fe-git OR roles:gstg-base-be-sidekiq OR roles:gstg-base-fe-web sudo /usr/local/bin/ha-ctl -l lb-1:lb-2 -c "sudo gitlab-ctl hup unicorn && sleep 3"
        bundle exec knife ssh -e -a fqdn roles:gstg-base-be-sidekiq sudo /opt/gitlab/init/sidekiq 1 || sudo /opt/gitlab/init/sidekiq-cluster 1
        bundle exec knife ssh -e -a fqdn roles:gstg-base-be-sidekiq sudo gitlab-ctl restart sidekiq-cluster
      COMMAND
    end
  end
end
